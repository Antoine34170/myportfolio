import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SnakeJsComponent } from './snake-js.component';

describe('SnakeJsComponent', () => {
  let component: SnakeJsComponent;
  let fixture: ComponentFixture<SnakeJsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SnakeJsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SnakeJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
