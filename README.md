# My Portfolio


## Documentation
Angular official documentation : https://angular.io/docs
Node.js official site : https://nodejs.org/en/
Bootstrap for Angular : https://ng-bootstrap.github.io/#/home
Classic Bootstrap : https://getbootstrap.com/

## Pre-requisite

### Node.js
You need node.js https://nodejs.org/en/download/ which includes npm (node packet manager) or other packet manager yarn as example, but i used npm (included with node.js)

### Angular-cli
You need to install Angular-cli -> https://angular.io/cli
then type "npm install -g @angular/cli" -g install globally angular commande line.

## Install Application 
### Pull project
- npm install -g @angular/cli (if not done before)
- git clone https://gitlab.com/Antoine34170/cv-restapi.git
- get into the folder pulled with cd myportfolio and type "npm install" it will download and install all "node_modules" used for the project to make it work
- always in directory folder type "npm run start" it will compile project then goes to "localhost:4200"

# ----------------------------------------------------------------------------------
# ----------------- Angular documentation automatically generated ------------------
# ----------------------------------------------------------------------------------


# MyPortfolio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.12.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
