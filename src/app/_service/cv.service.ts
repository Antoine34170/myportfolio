import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Database } from '../_model/database';
import { Experience } from '../_model/experience';
import { Formation } from '../_model/formation';
import { Framework } from '../_model/framework';
import { Language } from '../_model/language';
import { Misc } from '../_model/misc';


@Injectable({
  providedIn: 'root'
})
export class CvService {

  constructor(private http: HttpClient) {

  }

  private cvUrl = "http://localhost:8080/api"


  private log(log: string) {
    console.info(log);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
  // DATABASE 
  getDatabase(): Observable<Database[]> {
    const url = `${this.cvUrl}/database`;

    return this.http.get<Database[]>(url).pipe(
      tap(_ => this.log(`fetched database`)),
      catchError(this.handleError<Database[]>(`getDatabase `))
    );
  }

  // EXPERIENCE
  getExperience(): Observable<Experience[]> {
    const url = `${this.cvUrl}/experience`;

    return this.http.get<Experience[]>(url).pipe(
      tap(_ => this.log(`fetched experience`)),
      catchError(this.handleError<Experience[]>(`getExperience `))
    );
  }

  // FORMATION
  getFormation(): Observable<Formation[]> {
    const url = `${this.cvUrl}/formation`;

    return this.http.get<Formation[]>(url).pipe(
      tap(_ => this.log(`fetched formation`)),
      catchError(this.handleError<Formation[]>(`getformation `))
    );
  }
  // FRAMEWORK
  getFramework(): Observable<Framework[]> {
    const url = `${this.cvUrl}/framework`;

    return this.http.get<Framework[]>(url).pipe(
      tap(_ => this.log(`fetched framework`)),
      catchError(this.handleError<Framework[]>(`getframework `))
    );
  }
  // LANGUAGE
  getLanguage(): Observable<Language[]> {
    const url = `${this.cvUrl}/language`;

    return this.http.get<Language[]>(url).pipe(
      tap(_ => this.log(`fetched language`)),
      catchError(this.handleError<Language[]>(`getlanguage `))
    );
  }

  // MISC
  getMisc(): Observable<Misc[]> {
    const url = `${this.cvUrl}/misc`;

    return this.http.get<Misc[]>(url).pipe(
      tap(_ => this.log(`fetched misc`)),
      catchError(this.handleError<Misc[]>(`getmisc `))
    );
  }


}
