import { Formation } from "./formation";

export const FORMATION: Formation[] = [
    new Formation(1, "Formation Concepteur Développeur d'Applications", "Jan 2021 Nov 2021", ["Framework MVC N-tiers", "Conception de bases de données", "Gestion de projets"]),
    new Formation(2, "Formation Autodidacte", "Nov 2019 Jan 2021",["Framework : Angular, Spring, Hibernate","Langages : Javascript, Typescript, Java, Kotlin","Misc : Docker, PGSQL, Git"]),
    new Formation(3, "BTS Informatique de Gestion - option administrateur de réseaux locaux", "2009",["VLAN, routage NAT/PAT, DNS, DHCP"])

]
    // - Framework MVC N-tiers
    // - Conception de bases de données
    // - Gestion de projets

//     - Framework : Angular, Spring, Hibernate
// - Langages : Javascript, Typescript, Java, Kotlin
//     - Misc : Docker, PGSQL, Git