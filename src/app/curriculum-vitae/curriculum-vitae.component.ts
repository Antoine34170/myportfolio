import { Component, OnInit } from '@angular/core';
import { Database } from '../_model/database'
import { Framework } from '../_model/framework';
import { Experience } from '../_model/experience';
import { Formation } from '../_model/formation';
import { Language } from '../_model/language';
import { Misc } from '../_model/misc';
import { CvService } from '../_service/cv.service';


@Component({
  selector: 'app-curriculum-vitae',
  templateUrl: './curriculum-vitae.component.html',
  styleUrls: ['./curriculum-vitae.component.css']
})
export class CurriculumVitaeComponent implements OnInit {

  database: Database[] | undefined
  experience: Experience[] | undefined
  formation: Formation[] | undefined
  framework: Framework[] | undefined
  language: Language[] | undefined
  misc: Misc[] | undefined

  constructor(
    private cvService: CvService
  ) { }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData(): void {
    this.getDatabase();
    this.getExperience();
    this.getFormation();
    this.getFramework();
    this.getLanguage();
    this.getMisc();
  }

  getDatabase(): void {
    this.cvService.getDatabase()
      .subscribe(res => this.database = res)
  }

  getExperience(): void {
    this.cvService.getExperience()
      .subscribe(res => this.experience = res)
  }

  getFormation(): void {
    this.cvService.getFormation()
      .subscribe(res => this.formation = res)
  }

  getFramework(): void {
    this.cvService.getFramework()
      .subscribe(res => this.framework = res)
  }

  getLanguage(): void {
    this.cvService.getLanguage()
      .subscribe(res => this.language = res)
  }

  getMisc(): void {
    this.cvService.getMisc()
      .subscribe(res => this.misc = res)
  }


}
