import { Database } from './database';

export const DATABASES: Database[] = [
    new Database(1, "PostgreSQL", "../../assets/postgresql.png"),
    new Database(2, "Mysql", "../../assets/mysql.png")
    ]; 