import { Experience } from "./experience";

export const EXPERIENCE: Experience[] = [
    new Experience(1, "Technicien Support N2", "Sept 2018 Dec 2020", ["Formation, accompagnement et support sur les outils huissiers dejustice", "Documentation technique / utilisateur"]),
    new Experience(2, "Technicien Support", "Mars 2018 - Sept 2018", ["Formation, accompagnement et support sur les outils notaires", "Virtualisation (Hyper-V), Messagerie (Exchange), Sauvegarde (ArcServe)"]),
    new Experience(3, "Technicien Retail Itinérant", "Jan 2017- Février 2018", ["Installation, formation & SAV bornes bancaires","SAV Matériel Informatique"]),
    new Experience(4, "Technicien Support", "Sept 2015- Jan 2017", ["Formation, accompagnement et support logiciel Backoffice / E-commerce","Diverses problématiques réseaux"]),
    new Experience(5, "Technicien Support", "Juin 2012- Mai 2014", ["Installation, formation et SAV systèmes d'encaissements àdistance/sur site"])

]
    // - Framework MVC N-tiers
    // - Conception de bases de données
    // - Gestion de projets

//     - Framework : Angular, Spring, Hibernate
// - Langages : Javascript, Typescript, Java, Kotlin
//     - Misc : Docker, PGSQL, Git