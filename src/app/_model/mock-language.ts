import { Language } from './language';

export const LANGUAGES: Language[] = [
    new Language(1, "Javascript", "../../assets/javascript.png"),
    new Language(2, "Typescript", "../../assets/typescript.png"),
    new Language(3, "Java", "../../assets/java.png"),
    new Language(4, "Kotlin", "../../assets/kotlin.png"),
    new Language(3, "PHP", "../../assets/php.png")
];