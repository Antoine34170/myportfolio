export class Experience {
    id: number;
    name: string;
    beginDate: Date;
    endDate: Date
    details: Array<string>;

    constructor(id: number, name: string, beginDate: Date,endDate : Date, details: Array<string>) {
        this.id = id;
        this.name = name;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.details = details
    }
}
