import { Framework } from "./framework";

export const FRAMEWORK: Framework[] = [
    new Framework(1, "Angular", "../../assets/angular.png"),
    new Framework(2, "Spring", "../../assets/spring.png"),
    new Framework(3, "Hibernate", "../../assets/hibernate.png"),
    new Framework(4, "Bootstrap", "../../assets/bootstrap.png")
]

