import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsLinksComponent } from './projects-links.component';

describe('ProjectsLinksComponent', () => {
  let component: ProjectsLinksComponent;
  let fixture: ComponentFixture<ProjectsLinksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectsLinksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
