import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { ProjectsLinksComponent } from './projects-links/projects-links.component'
import { CurriculumVitaeComponent } from './curriculum-vitae/curriculum-vitae.component'
import { SnakeJsComponent } from './snake-js/snake-js.component';


const routes: Routes = [
  { path: '', component: CurriculumVitaeComponent },
  // { path: 'home', component: HomeComponent },
  { path: 'curriculum-vitae', component: CurriculumVitaeComponent },
  // { path: 'projects', component: ProjectsLinksComponent },
  // { path: 'snake-js', component: SnakeJsComponent },
 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
