# My Portfolio


### Documentation
- Angular official documentation : https://angular.io/docs  
- Node.js official site : https://nodejs.org/en/  
- Bootstrap for Angular : https://ng-bootstrap.github.io/#/home   
- Classic Bootstrap : https://getbootstrap.com/  

## Pre-requisite

### Node.js
You need node.js https://nodejs.org/en/download/ which includes npm (node packet manager) or other packet manager yarn as example, but i used npm (included with node.js) <br />

### Angular-cli
- You need to install Angular-cli -> https://angular.io/cli  
- then type "npm install -g @angular/cli" -g install globally angular commande line  

## Install Application 
### Pull project
- npm install -g @angular/cli (if not done before)  
- git clone https://gitlab.com/Antoine34170/cv-restapi.git  
- get into the - folder pulled with cd myportfolio and type "npm install" it will download and install all "node_modules" used for the project to make it work  
- always in directory folder type "npm run start" it will compile project then goes to "localhost:4200"   





